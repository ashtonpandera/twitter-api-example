FROM ruby:2.5

WORKDIR /opt/Pandera/firehose
COPY firehose.rb .

ENV target ''

RUN gem install twitter neo4j

CMD ruby firehose.rb ${target}