require 'twitter'
require 'neo4j'
require 'net/http'

# Module to handle the tweet labeling
module TweetLabel
  def self.initialize(user, pass)
    @neo = Neo4j::Session.open :server_db, "http://#{user}:#{pass}@localhost:7474"
  end

  def self.rate_tweets
    @neo.query('MATCH (t:Tweet) where size(labels(t)) = 1 return t limit 2000').each do |tweet|
      id   = tweet.t.get_property 'id'
      text = tweet.t.get_property 'text'
      next if text.nil?
      bad  = %w[dick sex cartel fuck].collect { |word| text.include? word }.select { |x| x }
      @neo.query "MATCH (t:Tweet {id: #{id}}) SET t:#{bad.empty? ? 'Ok' : 'Bad'}"
    end
  end
end

# Class to handle the twitter fire hose
class FireHose

  def streaming(topics)
    @api = Twitter::Streaming::Client.new do |config|
      config.consumer_key        = ''
      config.consumer_secret     = ''
      config.access_token        = ''
      config.access_token_secret = ''
    end

    topics = ['from:']
    @api.filter(track: topics.join(',')) do |tweet|
      puts tweet.text if tweet.is_a?(Twitter::Tweet)
      add_user tweet.user
      add_tweet tweet
    end
  end

  def initialize(user, pass)
    @api = Twitter::REST::Client.new do |config|
      config.consumer_key        = ''
      config.consumer_secret     = ''
      config.access_token        = ''
      config.access_token_secret = ''
    end

    @neo = Neo4j::Session.open :server_db, "http://#{user}:#{pass}@localhost:7474"
    # @neo = Neo4j::Session.open :server_db, "http://#{user}:#{pass}@docker.for.mac.localhost:7474"
  end

  def track(obj)
    if obj.is_a? String
      @target = add_user @api.user obj
      add_tweets @target
      add_follows(@target).each(&method(:add_tweets))
    elsif obj.is_a? Array
      streaming(obj)
    end
  end

  def add_user(user)
    puts "MERGE (a:Tweeter {screen_name: '#{user.screen_name}', id: #{user.id}}) return a"
    @neo.query "MERGE (a:Tweeter {screen_name: '#{user.screen_name}', id: #{user.id}}) return a"
    user
  end

  def add_tweets(user)
    user  = @api.user user unless user.is_a? Twitter::User
    since = @neo.query("match (:Tweeter {id:#{user.id}})-[]-(t:Tweet) return max(t.created_at) as max").first.max
    @api.search("from:#{user.id} #{"since:#{since[0..9]}" if since}").each do |tweet|
      puts "#{tweet.quote?}, #{tweet.retweet?}, #{tweet.reply?}, #{tweet.text}"
      add_quote tweet if tweet.quote?
      add_tweet tweet if tweet.retweet? or (not tweet.quote? and not tweet.reply?)
      add_reply tweet if tweet.reply?
    end
  end

  def add_tweet(tweet)
    original = tweet.retweeted_tweet.is_a?(Twitter::Tweet) ? tweet.retweeted_tweet : tweet
    @neo.query "merge (tweeter:Tweeter {id: #{tweet.user.id}, screen_name: #{tweet.user.screen_name}) "\
              "merge (tweet:Tweet {id: #{original.id}, text: {text}, created_at: {t_created_at}}) "\
              "merge (tweeter)-[:#{tweet.retweet? ? 'RE' : ''}TWEETED {created_at: {created_at}}]->(tweet)",
               created_at: tweet.created_at.strftime('%FT%T'), t_created_at: original.created_at.strftime('%FT%T'),
               text:       original.text
  end

  def add_quote(tweet)
    @neo.query "merge (tweeter:Tweeter {id: #{tweet.user.id}, screen_name: #{tweet.user.screen_name}) "\
              "merge (tweet:Tweet {id: #{tweet.id}, text: {text}, created_at: {created_at}}) "\
              "merge (quoted:Tweet {id: #{tweet.quoted_tweet.id}, text: {q_text}, created_at: {q_created_at}}) "\
              "merge (tweeter)-[:TWEETED {created_at: {created_at}}]-(tweet)-[:QUOTES]-(quoted)",
               created_at:   tweet.created_at.strftime('%FT%T'), text: tweet.text,
               q_created_at: tweet.quoted_tweet.created_at.strftime('%FT%T'), q_text: tweet.quoted_tweet.text
  end

  def add_reply(tweet)
    puts "#{tweet.in_reply_to_status_id}"
    @neo.query "merge (tweeter:Tweeter {id: #{tweet.user.id}, screen_name: #{tweet.user.screen_name}) "\
              "merge (tweet:Tweet {id: #{tweet.id}, text: {text}, created_at: {created_at}, "\
              "       at_user_id: {uid}, at_tweet_id: {tid}, at_user_screen_name: {usn} }) "\
              "merge (original:Tweet {id: {tid}}) "\
              "merge (tweeter)-[:TWEETED {created_at: {created_at}}]->(tweet)-[:REPLYING_TO]->(original) ",
               created_at: tweet.created_at.strftime('%FT%T'), text: tweet.text, tid: tweet.in_reply_to_tweet_id,
               uid:        tweet.in_reply_to_user_id, usn: tweet.in_reply_to_screen_name
  end

  def add_follows(user)
    @api.friends(@target.screen_name).collect do |friend|
      fresh   = @neo.query("match (t:Tweeter {id: #{friend.id}}) return t").first.nil? ? friend : false
      tweeter = "tweeter:Tweeter {id: #{user.id}}"
      friend  = "friend:Tweeter {screen_name: '#{friend.screen_name}', id: #{friend.id}}"
      @neo.query "match (#{tweeter}) merge (#{friend}) merge (tweeter)-[:FOLLOWED]->(friend)"
      fresh
    end.select { |x| x }
  end
end

# Set up database connection
tweet_label = TweetLabel
tweet_label.initialize('neo4j', 'test')

# Set up non Streaming Tweeter API connection.
fire_hose = FireHose.new 'neo4j', 'test'
# Run add tweets for user.
fire_hose.add_tweets 'twitterhandle'

# Do tweet rating for unrated tweets..
tweet_label.rate_tweets


